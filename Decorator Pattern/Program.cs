﻿using System;

namespace Grand_MOBILE_DECORATOR
{
    class Program
    {
        static void Main(string[] args)
        {
            Mobile myMobile = new Samsort();
            Console.WriteLine(myMobile.GetPrice());
            Console.WriteLine(myMobile.GetDescription());
            myMobile = new DualSim(myMobile);
            Console.WriteLine(myMobile.GetPrice());
            Console.WriteLine(myMobile.GetDescription());
            myMobile = new Bluetooth(myMobile);
            Console.WriteLine(myMobile.GetPrice());
            Console.WriteLine(myMobile.GetDescription());
        }
    }
}

public abstract class Mobile
{
    public abstract string GetDescription();

    public abstract double GetPrice();

    public string Description { get; set; }
}

public class Nofia : Mobile
{
    public Nofia()
    {
        Description = "Nofia Mobile";
    }

    public override string GetDescription()
    {
        return Description;
    }

    public override double GetPrice()
    {
        return 104.10;
    }
}

public class Samsort : Mobile
{
    public Samsort()
    {
        Description = "Samsort Mobile";
    }

    public override string GetDescription()
    {
        return Description;
    }

    public override double GetPrice()
    {
        return 113.45;
    }
}

public class DualSim : MobileDecorator
{
    public DualSim(Mobile mobile) : base(mobile)
    {
        Description = "Dual Sim";
    }

    public override string GetDescription()
    {
        return Mobile.GetDescription() + ", " + Description;
    }

    public override double GetPrice()
    {
        return Mobile.GetPrice() + 1.23;
    }
}

public class Bluetooth : MobileDecorator
{
    public Bluetooth(Mobile mobile)
        : base(mobile)
    {
        Description = "Bluetooth";
    }

    public override string GetDescription()
    {
        return Mobile.GetDescription() + ", " + Description;
    }

    public override double GetPrice()
    {
        return Mobile.GetPrice() + 0.35;
    }
}

public class MobileDecorator : Mobile
{
    protected Mobile Mobile;

    public MobileDecorator(Mobile mobile)
    {
        Mobile = mobile;
    }

    public override string GetDescription()
    {
        return Mobile.GetDescription();
    }

    public override double GetPrice()
    {
        return Mobile.GetPrice();
    }
}