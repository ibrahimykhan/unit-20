﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    public class Phone
    {
        public string Ram { get; set; }
        public string Processor { get; set; }
        public string OS { get; set; }
        public string ScreenSize { get; set; }

        public Phone(string _Ram, string _Processor, string _OS, string _ScreenSize)
        {
            this.Ram = _Ram;
            this.Processor = _Processor;
            this.OS = _OS;
            this.ScreenSize = _ScreenSize;
        }

        public Phone()
        {

        }
    }

    public class PhoneBuilder
    {
        private Phone phone;
        public Phone SetOS(string os)
        {
            phone.OS = os;
            return phone;
        }
        public Phone SetRAM(string ram)
        {
            phone.Ram = ram;
            return phone;
        }
        public Phone SetProcessor(string processor)
        {
            phone.Processor = processor;
            return phone;
        }
        public Phone SetScreenSize(string screenSize)
        {
            phone.ScreenSize = screenSize;
            return phone;
        }

        public void GetPhone()
        {
            Console.WriteLine("Phone details are : OS=" + phone.OS + ", RAM=" + phone.Ram);
        }

        public PhoneBuilder()
        {
            phone = new Phone();
        }
    }

    class Program
    {
        public static void Main()
        {
            Phone ph1 = new Phone("1GB", "2.5Ghz", "biryani", "");
            PhoneBuilder ph2 = new PhoneBuilder();
            ph2.SetOS("Andriod");
            ph2.SetRAM("1GB");
            ph2.GetPhone();
            Console.ReadKey();
        }
    }
}